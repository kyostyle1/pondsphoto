module.exports = {
  // Secret key for JWT signing and encryption
  secret: 'EdMBnILbPgcPcdrZuljeMsLecwax9a42',
  // Database connection information
  database: 'mongodb://localhost:27017',
  // Setting port for server
  port: 3000,
  // Configuring Mailgun API for sending transactional email
  mailgun_priv_key: 'mailgun private key here',
  // Configuring Mailgun domain for sending transactional email
  mailgun_domain: 'mailgun domain here',
  // Mailchimp API key
  mailchimpApiKey: 'mailchimp api key here',
  test_port: 3000,
  test_db: 'english',
  test_env: 'test'
};