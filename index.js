// Inporting Node Modules and Inittial Express
const express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  logger = require("morgan"),
  mongoose = require("mongoose"),
  config = require("./config/main");
const fs = require("fs");

let server;
if (process.env.NODE_ENV != config.test_env) {
  server = app.listen(config.port);
  console.log(`Your server is running on port ${config.port}.`);
} else {
  server = app.listen(config.test_port, { transport: ["websocket"] });
}

// Set static file location for production
app.set("view engine", "pug");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger("dev")); // Log requests to API using morgan

// Enable CORS from client-side
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials"
  );
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/home", (req, res) => {
  res.render("pug/home_1");
});

app.get("/home2", (req, res) => {
  res.render("pug/home_2");
});
app.get("/home3", (req, res) => {
  res.render("pug/home_3");
});
app.get("/home5", (req, res) => {
  res.render("pug/home_5");
});
app.get("/home6", (req, res) => {
  res.render("pug/home_6");
});
app.get("/home7", (req, res) => {
  res.render("pug/home_7");
});
app.get("/home8", (req, res) => {
  res.render("pug/home_8");
});
app.get("/home9", (req, res) => {
  res.render("pug/home_9");
});

app.post("/uploads", (req, res) => {
  const imgData = req.body.image_upload;
  let base64Data = imgData.replace(/^data:image\/png;base64,/, "");
  base64Data += base64Data.replace("+", " ");
  binaryData = new Buffer(base64Data, "base64").toString("binary");

  fs.writeFile("image1.png", binaryData, "binary", function(err) {
    console.log(err);
  });

  res.render("index");
});

// necessary for testing
module.exports = server;
