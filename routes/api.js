// importing node modules
const express = require('express');
const passport = require('passport');
const apiRoute = express.Router();

const AuthenticationController = require('../controllers/AuthenticationController');
const requireLogin = passport.authenticate('local', { session: false });
const requireAuth = passport.authenticate('jwt', { session: false });

apiRoute.get('/test', function (req, res) {
    res.render('user/index');
});

apiRoute.get('/home', function(req, res) {
    res.send("Home");
});

apiRoute.post('/login', requireLogin, AuthenticationController.login);

apiRoute.post('/login-as-facebook', function(req, res) {

});

module.exports = apiRoute;