// importing node modules
const express = require('express');
const passport = require('passport');
const AuthenticationController = require('../controllers/AuthenticationController');

const requireLogin = passport.authenticate('local', {session: false});
const requireAuth = passport.authenticate('jwt', {session: false});

const authRoute = express.Router();

// api/register
authRoute.post('/register', AuthenticationController.register);

// api/register_facebook
authRoute.post('/register-facebook', AuthenticationController.registerFacebook)

// api/login
authRoute.post('/login', requireLogin, AuthenticationController.login);

// api/forgot-password
authRoute.post('/forgot-password', AuthenticationController.forgotPassword);

// api/reset-token
authRoute.post('/reset-password/:token', AuthenticationController.verifyToken);

module.exports = authRoute;