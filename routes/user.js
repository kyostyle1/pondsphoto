// importing node modules
const express = require('express');
const passport = require('passport');
const AuthenticationController = require('../controllers/AuthenticationController');
const UserController = require('../controllers/UserController');
const requireLogin = passport.authenticate('local', {session: false});
const requireAuth = passport.authenticate('jwt', {session: false});
const ROLE_ADMIN = require('../constants').ROLE_ADMIN;

const userRoutes = express.Router();

// api/user
userRoutes.get('/:userId', requireAuth, UserController.viewProfile);

userRoutes.get('/protected', requireAuth, (req, res) => {
    res.send({ content: 'Admin dashboard is working.' })
});

userRoutes.get('/admin-onlys', requireAuth, AuthenticationController.roleAuthorization(ROLE_ADMIN),(req, res) => {
    res.send({ content: 'Admin dashboard is working.' })
});

module.exports = userRoutes;