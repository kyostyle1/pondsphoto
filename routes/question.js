// importing node modules
const express = require('express');
const passport = require('passport');
const AuthenticationController = require('../controllers/AuthenticationController');
const QuestionController = require('../controllers/QuestionController');
const busboy = require('connect-busboy');
const multer = require('multer');
var storage = multer.diskStorage({
    destination: function(req, res, cb) {
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb){
        cb(null, file.originalname)
    }
})
const upload = multer({storage});

//const requireLogin = passport.authenticate('local', {session: false});

const questionRoutes = express.Router();
questionRoutes.use(busboy());
// question/
questionRoutes.get('/', QuestionController.getAllQuestion);

// question/create
questionRoutes.get('/create', QuestionController.createQuestion);

// question/import
questionRoutes.get('/import', QuestionController.importQuestionGet);

questionRoutes.post('/import', upload.single('csv'), QuestionController.importQuestionPost);

questionRoutes.get('/test', QuestionController.test);

questionRoutes.get('/test1', QuestionController.test1);

// question/:id
//questionRoutes.get('/:id', QuestionController.getQuestionDetail);

// question/
questionRoutes.post('/', QuestionController.postQuestion);

// question/
questionRoutes.put('/:id', QuestionController.editQuestion);

module.exports = questionRoutes;