// Importing Node Modules
const jwt = require('jsonwebtoken');
const cryto = require('crypto');
const User = require('../models/User');
const setUserInfo = require('../helpers.js').setUserInfo;
const setUserInfoFacebook = require('../helpers.js').setUserInfoFacebook;

const getInfo = require('../helpers').getRole;
const config = require('../config/main');

//
// Generate JWT Token
//
function generateToken(user) {
    return jwt.sign(user, config.secret, {
        expiresIn: 604800 // in seconds
    });
}

// = =====================================
// = Login Route =
// =======================================

exports.home = function(req, res, next) {
    res.render('hello');
}

// = =====================================
// = Login Route =
// =======================================

exports.login = function (req, res, next) {
    const userInfo = setUserInfo(req.user);
    res.status(200).json({token: `JWT ${generateToken(userInfo)}`, user: userInfo});
};

// = =====================================
// = Registration Route =
// =======================================
exports.register = function (req, res, next) {
    const email = req.body.email;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const password = req.body.password;

    console.log(req.body);
    // Return error if no email provided
    if (!email) {
        return res.status(422).send({error: 'You must enter an email'});
    }

    // Return error if full name not provided
    if (!firstName || !lastName) {
        return res.status(422).send({error: 'You must enter your full name.'});
    }

    // Return error if no password provided
    if (!password) {
        return res.status(422).send({error: 'You must enter a password.'});
    }

    User.findOne({email}, (err, existingUser) => {
        if (err) {
            return next(err);
        }

        // If user is not unique, return error
        if (existingUser) {
            return res.status(422).send({error: 'That email address is already in use.'});
        }

        const user = new User({
            email,
            password,
            profile: {
                firstName,
                lastName
            }
        });

        user.save((err, user) => {
            if (err) {
                return next(err);
            }
            // Subscribe member to Mailchimp list
            // mailchimp.subscribeToNewsletter(user.email); Respond with JWT if user was
            // created
            const userInfo = setUserInfo(user);
            res.status(200).json({token: `JWT ${generateToken(userInfo)}`, user: userInfo});
        });
    });
}

// = =====================================
// = Registration Facebook Route =
// =======================================
exports.registerFacebook = (req, res, next) => {
    console.log(req.body);
    const facebook_token = req.body.facebook_token;
    const name = req.body.name;
    const facebook_id = req.body.facebook_id;
    const facebook_avatar = req.body.facebook_avatar;

    if (!facebook_token) {
        return res.status(422).send({error: 'You must enter an token'});
    }

    if (!name || !facebook_id) {
        return res.status(422).send({error: 'You must enter your full name and facebook_id'});
    }

    User.findOne({facebook_id}, (err, existingFacebookId) => {
        if(err) {
            return next(err);
        }

        if(existingFacebookId) {
            return res.status(422).send({error: 'That facebook token is already in use.'});
        }

        const user = new User({
            facebook_token,
            name,
            facebook_id,
            facebook_avatar
        });

        user.save((err, user) => {
            if (err) {
                return next(err);
            }
            const userInfo = setUserInfoFacebook(user);
            res.status(200).json({token: `JWT ${generateToken(userInfo)}`, user: userInfo})
        });
    })
}

// = =====================================
// = Forgot Password Route =
// =======================================
exports.forgotPassword = (req, res) => {
    res.status(200).json({success: "OK"});
}

// = =====================================
// = Verify Token Route =
// =======================================
exports.verifyToken = (req, res) => {
    res.status(200).json({success: "OK"});
}

///= =======================================
// Authorization Middleware
//= =======================================

exports.roleAuthorization = function (requiredRole) {
  return function (req, res, next) {
    const user = req.user;

    User.findById(user._id, (err, foundUser) => {
      if (err) {
        res.status(422).json({ error: 'No user was found.' });
        return next(err);
      }

      // If user is found, check role.
      if (getRole(foundUser.role) >= getRole(requiredRole)) {
        return next();
      }

      return res.status(401).json({ error: 'You are not authorized to view this content.' });
    });
  };
};