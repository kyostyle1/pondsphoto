const Question = require('../models/Question');
var fs = require('fs');
var csv = require('fast-csv');
var jsonfile = require('jsonfile');
var csvData = require('../uploads/Economy1.json');
var trim = require('trim');
var Jimp = require("jimp");
var gm = require("gm");
let imageMagick = gm.subClass({imageMagick: true});
var cloudinary = require('cloudinary');

exports.getAllQuestion = (req, res) => {
    Question.find({}, (err, questions) => {
        res.render('question/index', {questions});
    });    
}

exports.importQuestionGet = (req, res) => {
    res.render('question/import');
}

exports.test1 = (req, res) => {
    cloudinary.config({ 
        cloud_name: 'dp10vxy5q', 
        api_key: '819138548849172', 
        api_secret: '10nfFl7BtYa3mvgTWbyHCTlYX9Q' 
    });

    cloudinary.image("../1234.png.jpg", 
        {transformation: [
            {width: 500, crop: "scale"},
            {overlay: "text:Arial_80:Flowers"}
        ]}
    );
    res.status(200).json({success: "success"});
}

function firstToUpperCase( str ) {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
}

function measureText(font, text) {
    var x = 0;
    for (var i = 0; i < text.length; i++) {
        if (font.chars[text[i]]) {
            x += font.chars[text[i]].xoffset
                + (font.kernings[text[i]] && font.kernings[text[i]][text[i + 1]] ? font.kernings[text[i]][text[i + 1]] : 0)
                + (font.chars[text[i]].xadvance || 0);
        }
    }
    return x;
};

exports.test = (req, res) => {
    let file = jsonfile.readFile('./uploads/Economy1.json', (err, obj) => {
        let data = [];
        let records = obj['RECORDS'];
        
        records.map((value) => {
            new Promise((resolve, reject) => {
                let question = trim(value['NoiDung'].replace(/\s{2,9999}|\t/g, ' ')
                    .replace(/\.{4,}/g,'______')
                    .replace(/\-{2,}/g,'______')
                    .replace(/\…{1,}/g,'______')
                );
                resolve(question);
            }).then((question) => {
                let matchs1 = question.match(/[a-zA-Z]_{6,6}/g);
                if(matchs1 != null) {
                    let stringTemp = matchs1[0].split("______");
                    let stringJoin = `${stringTemp[0]} ______`;
                    question.replace(/[a-zA-Z]_{6,6}/g, stringJoin);
                }
                return question;
            }).then((question) => {
                let matchs = question.match(/_{6,6}[a-zA-Z]/g);
                let answer = [
                    firstToUpperCase(trim(value['CauA'])),
                    firstToUpperCase(trim(value['CauB'])),
                    firstToUpperCase(trim(value['CauC'])),
                    firstToUpperCase(trim(value['CauD']))
                ];
                let correctAnswer = (answer) => {
                    switch(value['DapAn']) {
                        case 'A':
                            return trim(answer[0]);
                            break;
                        case 'B':
                            return trim(answer[1]);                    
                            break;
                        case 'C':
                            return trim(answer[2]);                    
                            break;
                        case 'D':
                            return trim(answer[0]);                    
                            break;
                    }
                };
                if(matchs !== null) {
                    let stringTemp = matchs[0].split("______");
                    let stringJoin = `______ ${stringTemp[1]}`;
                    question.replace(/_{6,6}[a-zA-Z]/g, stringJoin);
                }
                let category = value['ChuDe'];
                data.push({question, answer, correctAnswer: correctAnswer(answer) ,category});       
                return ({question, answer, correctAnswer: correctAnswer(answer) ,category});         
            }).then( (value) => {
                console.log(value.question);
                Question.findOne({question: value.question}, (err, existingQuestion) => {
                    if (err) {
                        return;
                    }
                    // If question is not unique, by pass
                    if (existingQuestion) {
                        return;
                    }
                    const question = new Question({
                        question: value.question,
                        answer: value.answer,
                        correctAnswer: value.correctAnswer,
                        category: value.category,
                        level: 1,
                        type: 'TEXT'
                    });
    
                    question.save((err, question) => {
                        if (err) {
                            return;
                        } else {
                            return question
                        }
                    });
                });
            });
        })
        res.status(200).json({data: "OK"});                                            
    });
}

exports.importQuestionPost = (req, res, next) => {
    fs.createReadStream('uploads/' + req.file.originalname)
        .pipe(csv())
        .on('data', (data) => {
            console.log(data[0]);
            Question.findOne({question: data[0]}, (err, existingQuestion) => {
                if (err) {
                    return;
                }

                // If question is not unique, by pass
                if (existingQuestion) {
                    return;
                }

                const question = new Question({
                    question: data[0],
                    answer: [data[1], data[2], data[3], data[4]],
                    correctAnswer: data[5],
                    category: data[6],
                    level: data[7],
                    type: data[8],
                    image: data[9],
                    audio: data[10]
                });

                question.save((err, user) => {
                    if (err) {
                        return;
                    }
                });
            });
        })
        .on('end', (data) => {
            res.redirect('/question');
        });
}

exports.createQuestion = (req, res) => {
    res.render('question/create');
}

exports.getQuestionDetail = (req, res) => {
    res.send(req.params);
    //res.status(200).json({ question: Question.find()});
}

exports.postQuestion = (req, res) => {
    res.status(200).json({request: req.body});
}

exports.editQuestion = (req, res) => {
    res.status(200).json({request: req.params});
}