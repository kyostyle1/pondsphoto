const ROLE_MEMBER = 'Member';
const ROLE_CLIENT = 'Client';
const ROLE_OWNER = 'Owner';
const ROLE_ADMIN = 'Admin';
const VN = 'vn';
const EN = 'en';
const IMAGE = 'IMAGE';
const AUDIO = 'AUDIO';
const TEXT = 'TEXT';

module.exports = {
  ROLE_MEMBER,
  ROLE_CLIENT,
  ROLE_OWNER,
  ROLE_ADMIN,
  VN,
  EN
};

module.exports = {
  IMAGE,
  AUDIO,
  TEXT
}