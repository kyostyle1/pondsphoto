$(document).ready(function() {
  const video = document.getElementById("video");
  const canvas = document.getElementById("canvas");
  const context = canvas.getContext("2d", {preserveDrawingBuffer:true});
  let base64 = "";
  var image = new Image();
  image.src = "images/default.png";

  $(".picture-choosen").hide();
  $(".filter-image").hide();

  $("#snap").click(async function() {
    $(".camera_screen > .img").remove();
    image.onload = function() {
      context.drawImage(video, 0, 0, 720, 750);
    };
    image.src = canvas.toDataURL("image/png");
    document
      .getElementById("pngHolder")
      .appendChild(image);
    $(".shoot_button").hide();
    $(".picture-choosen").show();
    // $("#image_upload").val(base64);
  });

  //   $("#btn-confirm").click(function() {
  //     // var image_tag = $('#pngHolder > img');
  //     // var image_base64 = image_tag.attr('src');
  //     // $('#image_upload').val(image_base64);
  //     return new Promise((resolve, reject) => {
  //       let base64 = canvas.toDataURL("image/png");
  //       resolve(base64);
  //     })
  //       .then(base64 => {
  //         $("#image_upload").val(base64);
  //         $(".picture-choosen").hide();
  //         $(".filter-image").show();
  //         setTimeout(console.log('aa'), 5000);
  //     });
  //     // console.log(canvas.toDataURL("image/png"));

  //     // let base64 = canvas.toDataURL("image/png");
  //     // $('#image_upload').val(base64);

  //     // $('#form-upload').submit();
  //   });

  $("#btn-confirm").click(function() {
    $.ajax({
      type: "POST",
      url: "/uploads",
      data: {
        image_upload: base64
      }
    }).done(function(response) {
      console.log(base64);
    });
  });
});

// Converts canvas to an image
function convertCanvasToImage(canvas, context) {
  var image = new Image();
  image.src = "images/default.png";
  image.onload = function() {
    context.clearRect( 0, 0, 720, 750 )
    context.drawImage(video, 0, 0, 720, 750);
  };
  image.src = canvas.toDataURL("image/png", 1);
  return image;
}
